﻿using ClothBazar.entities;
using ClothBazar.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace ClothBazar.Web.Controllers
{
    public class CategoryController : Controller
    {
        CategoriesService categoryService = new CategoriesService();

        // GET: Category
        [HttpGet]
        public ActionResult Index()
        {
            var categories = categoryService.GetCategories();
            return View(categories);
        }

        // GET: Category
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Category category)
        {
            categoryService.SaveCategory(category);

            return View();
        }

        // GET: Category
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var category = categoryService.GetCategory(ID);
            return View(category);
        }

        [HttpPost]
        public ActionResult Edit(Category category)
        {
            try
            {
                var existingCat = categoryService.GetCategory(category.ID);
                string existingfile = existingCat.ImageURL;
                FileInfo file = new FileInfo(Server.MapPath("~/") + existingfile);
                if (file.Exists)
                {
                    file.Delete();
                }
            }
            catch (Exception)
            {

                throw;
            }

            categoryService.UpdateCategory(category);

            return RedirectToAction("Index");
        }
        // GET: Category
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            var category = categoryService.GetCategory(ID);
            return View(category);
        }

        [HttpPost]
        public ActionResult Delete(Category category)
        {
            try
            {
                var existingCat = categoryService.GetCategory(category.ID);
                string existingfile = existingCat.ImageURL;
                FileInfo file = new FileInfo(Server.MapPath("~/") + existingfile);
                if (file.Exists)
                {
                    file.Delete();
                }
            }
            catch (Exception)
            {

                throw;
            }
            categoryService.DeleteCategory(category.ID);
            return RedirectToAction("Index");
        }
    }
}