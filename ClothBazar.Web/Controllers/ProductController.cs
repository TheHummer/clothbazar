﻿using ClothBazar.entities;
using ClothBazar.Services;
using ClothBazar.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar.Web.Controllers
{
    public class ProductController : Controller
    {
        ProductsService productsService = new ProductsService();
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductTable(string Search)
        {
            var products = productsService.GetProducts();
            if (string.IsNullOrEmpty(Search) == false)
            { 
               //products = products.Where(m => String.IsNullOrWhiteSpace(m.Name) && m.Name.ToLower().Contains(Search.ToLower())).ToList();
                products = products.Where(m => m.Name != null && m.Name.ToLower().Contains(Search.ToLower())).ToList();
            }
            return PartialView(products);
        }

        // GET: Category
        [HttpGet]
        public ActionResult Create()
        {
            CategoriesService CategoryService = new CategoriesService();
            var Categories = CategoryService.GetCategories();
            return PartialView(Categories);
        }

        [HttpPost]
        public ActionResult Create(NewCategoryViewModels model)
        {
            CategoriesService CategoryService = new CategoriesService();
            var newProduct = new Product();
            newProduct.Name = model.Name;
            newProduct.Description = model.Description;
            newProduct.Price = model.Price;
            newProduct.Category = CategoryService.GetCategory(model.CategoryID);
            productsService.SaveProduct(newProduct);

            return RedirectToAction("ProductTable");
        }

        // GET: Category
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var product = productsService.GetProduct(ID);
            return PartialView(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            productsService.UpdateProduct(product);

            return RedirectToAction("ProductTable");
        }

        [HttpPost]
        public ActionResult Delete(int ID)
        {
            productsService.DeleteProduct(ID);

            return RedirectToAction("ProductTable");
        }
    }
}
